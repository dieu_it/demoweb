+++
title = "LIÊN HỆ"
id = "contact"
+++

## Chúng tôi có thể giúp được gì cho bạn

Anh/chị có bất kỳ câu hỏi, thắc mắc, hoặc phản hồi nào về sản phẩm dịch vụ của chúng tôi cũng như lĩnh vực CNTT, tích hợp, smart-city,… Hãy điền thông tin vào form dưới!



##### Điện thoại: (84 – 236) 357 5788
##### Email: support@tpsc.vn
##### Địa chỉ: 49 Lê Quý Đôn, P. Bình Thuận, Q. Hải Châu, TP. Đà Nẵng